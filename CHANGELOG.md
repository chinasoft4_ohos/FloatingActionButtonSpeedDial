## 1.0.2
ohos 优化版本
 * 修改工程目录结构

## 1.0.1
ohos 优化版本
 * 优化了菜单展开动画交互
 * 新增FloatingActionButton阴影效果
 * 优化了FloatingActionButton旋转动画效果
 * 优化了SnackBar显示效果

## 1.0.0
ohos 第三个版本
 * 正式版本

## 0.0.2-SNAPSHOT
 * ohos 第二个版本，修复了findbugs问题,更新SDK6

## 0.0.1-SNAPSHOT
 * ohos第一个版本，完整实现了原库的全部api
 * 因为鸿蒙暂不支持ListContainer动画展开效果,按钮展开动画未实现，不影响主功能。