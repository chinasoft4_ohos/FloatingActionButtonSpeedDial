# FloatingActionButtonSpeedDial

#### 项目介绍
- 项目名称：FloatingActionButtonSpeedDial
- 所属系列：openharmony的第三方组件适配移植
- 功能：ohos库提供了Material Design浮动动作按钮快速拨号的实现
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio 3.0 Beta1
- 基线版本：Release 3.2.0

#### 效果演示
<img src="https://gitee.com/chinasoft4_ohos/FloatingActionButtonSpeedDial/raw/master/img/demo.gif"></img>

#### 安装教程
1.在项目根目录下的build.gradle文件中
 ```gradle
allprojects {
    repositories {
        maven {
            url 'https://s01.oss.sonatype.org/content/repositories/releases/'
        }
    }
}
 ```
2.在app模块的build.gradle文件中
 ```gradle
 dependencies {
     implementation('com.gitee.chinasoft_ohos:FloatingActionButtonSpeedDial:1.0.2')
     ......  
  }
 ```
在sdk6，DevEco Studio 3.0 Beta1下项目可直接运行,

如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，

并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明

1.xml布局
```xml
    <DependentLayout
        ohos:height="match_parent"
        ohos:width="match_parent"
        >

        <ListContainer
            ohos:id="$+id:listContainer"
            ohos:height="match_parent"
            ohos:width="match_parent"
            ohos:background_element="#FFFFFFFF"
            ohos:long_click_enabled="false"
            />

        <com.leinardi.ohos.speeddial.SpeedDialOverlayLayout
            ohos:id="$+id:overlay"
            ohos:width="match_parent"
            ohos:height="match_parent"
            app:clickable_overlay="true"/>

        <com.leinardi.ohos.speeddial.SpeedDialView
            ohos:id="$+id:SpeedDialView"
            ohos:height="match_content"
            ohos:width="match_content"
            ohos:align_parent_bottom="true"
            ohos:align_parent_right="true"
            ohos:margin="2vp"
            app:sdMainFabAnimationRotateAngle="90"
            app:sdMainFabClosedSrc="$graphic:ic_add_white_24dp"
            app:sdMainFabOpenedSrc="$graphic:ic_pencil_alt_white_24dp"
            app:sdOverlayLayout="$id:overlay"
            />

    </DependentLayout>
```

2.Java Api方法调用
```java
        SpeedDialView mSpeedDialView = (SpeedDialView) findComponentById(ResourceTable.Id_SpeedDialView);
        
        // 添加ActionItem
        PixelMapElement element = new PixelMapElement(getResourceManager().getResource(ResourceTable.Media_ic_custom_color));
        FabWithLabelView fabWithLabelView = mSpeedDialView.addActionItem(new SpeedDialActionItem.Builder(ResourceTable.Integer_fab_custom_color, element) // 初始化构造
                .setFabSize(FloatingActionButton.SIZE_NORMAL) // 设置FloatingActionButton大小，目前支持两种大小SIZE_NORMAL和SIZE_MINI
                .setFabBackgroundColor(UiUtils.getColor(getContext(), ResourceTable.Color_material_green_500)) // 设置FloatingActionButton背景色
                .setLabel(ResourceTable.String_label_custom_color) // 设置标签名称
                .setLabelColor(Color.WHITE.getValue()) // 设置标签字体颜色
                .setLabelBackgroundColor(UiUtils.getColor(getContext(), ResourceTable.Color_inbox_primary)) // 设置标签背景色
                .create());


        // 修改已经添加的ActionItem
        SpeedDialActionItem mActionItem = fabWithLabelView.getSpeedDialActionItemBuilder()
                .setFabBackgroundColor(UiUtils.getColor(this, ResourceTable.Color_material_white_1000))
                .create();
        fabWithLabelView.setSpeedDialActionItem(mActionItem);
        
        // 交互事件监听
        mSpeedDialView.setOnChangeListener(new SpeedDialView.OnChangeListener() {
            @Override
            public boolean onMainActionSelected() {
                // 主FloatingActionButton被点击
                showToast("Main action clicked!");
                return false;
            }

            @Override
            public void onToggleChanged(boolean isOpen) {
                // 主FloatingActionButton开关状态
            }
        });
        
        // ActionItem被点击交互监听
        mSpeedDialView.setOnActionSelectedListener(actionItem -> {
            // 根据不同ActionItem Id进行判断
            switch (actionItem.getId()) {
                case ResourceTable.Integer_xxx:
                    break;
                default:
                    break;
            }
            // true 表示一直打开状态，false 表示关闭
            return true;
        });
        
        // 与滚动组件配合交互
        ListContainer.addScrolledListener(new Component.ScrolledListener() {
            @Override
            public void onContentScrolled(Component component, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if(scrollY > oldScrollY) {
                    //上滑，消失
                    mSpeedDialView.hide();
                }else {
                    //下滑，显示
                    mSpeedDialView.show();
                }
            }

            @Override
            public void scrolledStageUpdate(Component component, int newStage) {
                
            }
        });
        
        // 设置浮层, 即FloatingActionButton打开菜单时的遮罩层
        mSpeedDialView.setOverlayLayout(SpeedDialOverlayLayout overlayLayout);
        
        // 设置展开方向，默认为TOP
        mSpeedDialView.setExpansionMode(SpeedDialView.ExpansionMode.TOP);
        
        // 设置FloatingActionButton icon旋转角度
        mSpeedDialView.setMainFabAnimationRotateAngle(90f);
        
        // 设置FloatingActionButton 打开时的背景色
        mSpeedDialView.setMainFabOpenedBackgroundColor(UiUtils.getPrimaryColor(speedDialView.getContext()));
        
        // 设置FloatingActionButton 关闭时的背景色
        mSpeedDialView.setMainFabClosedBackgroundColor(UiUtils.getPrimaryColor(speedDialView.getContext()));
        
        // 根据positin移除ActionItem
        mSpeedDialView.removeActionItem(2);
        
        // 根据ActionItem Id移除ActionItem
        mSpeedDialView.removeActionItemById(ResourceTable.Integer_fab_remove_action);
        
        // 添加ActionItem
        mSpeedDialView.addActionItem(new SpeedDialActionItem.Builder(
                        (int)System.currentTimeMillis(),
                        ResourceTable.Graphic_ic_pencil_alt_white_24dp).create()
                );
        
        // 替换ActionItem
        mSpeedDialView.replaceActionItem(new SpeedDialActionItem.Builder(ResourceTable.Integer_fab_remove_action, ResourceTable.Graphic_ic_delete_white_24dp)
                            .setLabel(getString(ResourceTable.String_label_remove_action))
                            .setFabBackgroundColor(UiUtils.getColor(this, ResourceTable.Color_inbox_accent))
                            .create(), ADD_ACTION_POSITION);
        
```

#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异

#### 版本迭代

- 1.0.2

#### 版权和许可信息
 ```
Copyright 2021 Roberto Leinardi.

Licensed to the Apache Software Foundation (ASF) under one or more contributor
license agreements.  See the NOTICE file distributed with this work for
additional information regarding copyright ownership.  The ASF licenses this
file to you under the Apache License, Version 2.0 (the "License"); you may not
use this file except in compliance with the License.  You may obtain a copy of
the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
License for the specific language governing permissions and limitations under
the License.
 ```