package com.leinardi.ohos.speeddial.sample;

import ohos.agp.components.*;
import ohos.app.Context;

public class CustomAdapter extends BaseItemProvider {
    private final Context context;

    public CustomAdapter(Context context){
        this.context = context;
    }

    @Override
    public int getCount() {
        return 60;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public Component getComponent(int i, Component convertComponent, ComponentContainer componentContainer) {
        final Component cpt;
        if (convertComponent == null) {
            cpt = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_list_item, null, false);
        } else {
            cpt = convertComponent;
        }
        Text text = (Text) cpt.findComponentById(ResourceTable.Id_text);
        text.setText("This is element #" + i);
        return cpt;
    }
}
