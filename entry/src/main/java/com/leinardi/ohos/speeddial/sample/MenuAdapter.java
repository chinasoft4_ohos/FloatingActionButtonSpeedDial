package com.leinardi.ohos.speeddial.sample;

import ohos.agp.components.*;
import ohos.app.Context;

import java.util.List;

public class MenuAdapter extends BaseItemProvider {
    private final Context context;
    private final List<MenuInfo> menuInfoList;

    public MenuAdapter(Context context, List<MenuInfo> menuInfoList) {
        this.context = context;
        this.menuInfoList = menuInfoList;
    }

    @Override
    public int getCount() {
        return menuInfoList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public Component getComponent(int i, Component convertComponent, ComponentContainer componentContainer) {
        MenuInfo menuInfo = menuInfoList.get(i);
        final Component cpt;
        if (convertComponent == null) {
            cpt = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_popmenu_item_layout, null, false);
        } else {
            cpt = convertComponent;
        }
        cpt.setId(menuInfo.getId());
        Text text = (Text) cpt.findComponentById(ResourceTable.Id_text);
        text.setText(menuInfo.getTitle());
        Image image = (Image) cpt.findComponentById(ResourceTable.Id_arrowImage);
        image.setVisibility(menuInfo.isMore() ? Component.VISIBLE : Component.HIDE);
        return cpt;
    }
}
