package com.leinardi.ohos.speeddial.sample;

public class MenuInfo {
    private int id;
    private String title;
    private boolean isMore;

    public MenuInfo(int id, String title, boolean isMore) {
        this.id = id;
        this.title = title;
        this.isMore = isMore;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isMore() {
        return isMore;
    }

    public void setMore(boolean more) {
        isMore = more;
    }
}
