package com.leinardi.ohos.speeddial.sample;

import java.util.ArrayList;
import java.util.List;

public class MenuInfoConstants {
    public static final List<MenuInfo> expandModeList = new ArrayList<MenuInfo>() {{
        add(new MenuInfo(ResourceTable.Integer_action_expansion_mode_top, "Top", false));
        add(new MenuInfo(ResourceTable.Integer_action_expansion_mode_left, "Left", false));
        add(new MenuInfo(ResourceTable.Integer_action_expansion_mode_bottom, "Bottom", false));
        add(new MenuInfo(ResourceTable.Integer_action_expansion_mode_right, "Right", false));
    }};

    public static final List<MenuInfo> rotateDegreeList = new ArrayList<MenuInfo>() {{
        add(new MenuInfo(ResourceTable.Integer_action_rotation_angle_0, "0 degrees", false));
        add(new MenuInfo(ResourceTable.Integer_action_rotation_angle_45, "45 degrees", false));
        add(new MenuInfo(ResourceTable.Integer_action_rotation_angle_90, "90 degrees", false));
        add(new MenuInfo(ResourceTable.Integer_action_rotation_angle_180, "180 degrees", false));
    }};

    public static final List<MenuInfo> moreList = new ArrayList<MenuInfo>() {{
        add(new MenuInfo(ResourceTable.Integer_action_main_fab_color, "Main FAB color", true));
        add(new MenuInfo(ResourceTable.Integer_action_toggle_list, "Toggle list", false));
        add(new MenuInfo(ResourceTable.Integer_action_toggle_reverse_animation, "Toggle reverse animation on close", false));
        add(new MenuInfo(ResourceTable.Integer_action_add_item, "Add action item", false));
        add(new MenuInfo(ResourceTable.Integer_action_remove_item, "Remove action item", false));
    }};

    public static final List<MenuInfo> mainFabColorList = new ArrayList<MenuInfo>() {{
        add(new MenuInfo(ResourceTable.Integer_action_main_fab_background_color_open, "Main FAB color open", true));
        add(new MenuInfo(ResourceTable.Integer_action_main_fab_background_color_close, "Main FAB color close", true));
    }};

    public static final List<MenuInfo> mainFabCloseColorList = new ArrayList<MenuInfo>() {{
        add(new MenuInfo(ResourceTable.Integer_action_main_fab_background_color_closed_primary, "Primary", false));
        add(new MenuInfo(ResourceTable.Integer_action_main_fab_background_color_closed_orange, "Orange", false));
        add(new MenuInfo(ResourceTable.Integer_action_main_fab_background_color_closed_purple, "Purple", false));
        add(new MenuInfo(ResourceTable.Integer_action_main_fab_background_color_closed_white, "White", false));
        add(new MenuInfo(ResourceTable.Integer_action_main_fab_background_color_closed_none, "None", false));
    }};

    public static final List<MenuInfo> mainFabOpenColorList = new ArrayList<MenuInfo>() {{
        add(new MenuInfo(ResourceTable.Integer_action_main_fab_background_color_opened_primary, "Primary", false));
        add(new MenuInfo(ResourceTable.Integer_action_main_fab_background_color_opened_orange, "Orange", false));
        add(new MenuInfo(ResourceTable.Integer_action_main_fab_background_color_opened_purple, "Purple", false));
        add(new MenuInfo(ResourceTable.Integer_action_main_fab_background_color_opened_white, "White", false));
        add(new MenuInfo(ResourceTable.Integer_action_main_fab_background_color_opened_none, "None", false));
    }};
}
