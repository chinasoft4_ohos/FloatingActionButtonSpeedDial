package com.leinardi.ohos.speeddial.sample;

import com.leinardi.ohos.speeddial.SpeedDialActionItem;
import com.leinardi.ohos.speeddial.SpeedDialView;
import com.leinardi.ohos.speeddial.UiUtils;

public class MenuUtils {

    public static void handlerMenuSetting(SpeedDialView speedDialView, long id) {
        switch ((int) id) {
            case ResourceTable.Integer_action_expansion_mode_top:
                speedDialView.setExpansionMode(SpeedDialView.ExpansionMode.TOP);
                break;
            case ResourceTable.Integer_action_expansion_mode_left:
                speedDialView.setExpansionMode(SpeedDialView.ExpansionMode.LEFT);
                break;
            case ResourceTable.Integer_action_expansion_mode_bottom:
                speedDialView.setExpansionMode(SpeedDialView.ExpansionMode.BOTTOM);
                break;
            case ResourceTable.Integer_action_expansion_mode_right:
                speedDialView.setExpansionMode(SpeedDialView.ExpansionMode.RIGHT);
                break;
            case ResourceTable.Integer_action_rotation_angle_0:
                speedDialView.setMainFabAnimationRotateAngle(0f);
                break;
            case ResourceTable.Integer_action_rotation_angle_45:
                speedDialView.setMainFabAnimationRotateAngle(45f);
                break;
            case ResourceTable.Integer_action_rotation_angle_90:
                speedDialView.setMainFabAnimationRotateAngle(90f);
                break;
            case ResourceTable.Integer_action_rotation_angle_180:
                speedDialView.setMainFabAnimationRotateAngle(180f);
                break;
            case ResourceTable.Integer_action_toggle_reverse_animation:
                speedDialView.setUseReverseAnimationOnClose(!speedDialView.getUseReverseAnimationOnClose());
                break;
            case ResourceTable.Integer_action_add_item:
                speedDialView.addActionItem(new SpeedDialActionItem.Builder(
                        (int)System.currentTimeMillis(),
                        ResourceTable.Graphic_ic_pencil_alt_white_24dp).create()
                );
                break;
            case ResourceTable.Integer_action_remove_item:
                int size = speedDialView.getActionItems().size();
                if (size > 0) {
                    speedDialView.removeActionItem(size - 1);
                }
                break;
            case ResourceTable.Integer_action_main_fab_background_color_closed_primary:
                speedDialView.setMainFabClosedBackgroundColor(UiUtils.getPrimaryColor(speedDialView.getContext()));
                break;
            case ResourceTable.Integer_action_main_fab_background_color_closed_orange:
                speedDialView.setMainFabClosedBackgroundColor(UiUtils.getColor(speedDialView.getContext(), ResourceTable.Color_material_orange_500));
                break;
            case ResourceTable.Integer_action_main_fab_background_color_closed_purple:
                speedDialView.setMainFabClosedBackgroundColor(UiUtils.getColor(speedDialView.getContext(), ResourceTable.Color_material_purple_500));
                break;
            case ResourceTable.Integer_action_main_fab_background_color_closed_white:
                speedDialView.setMainFabClosedBackgroundColor(UiUtils.getColor(speedDialView.getContext(), ResourceTable.Color_material_white_1000));
                break;
            case ResourceTable.Integer_action_main_fab_background_color_closed_none:
                speedDialView.setMainFabClosedBackgroundColor(0);
                break;
            case ResourceTable.Integer_action_main_fab_background_color_opened_primary:
                speedDialView.setMainFabOpenedBackgroundColor(UiUtils.getPrimaryColor(speedDialView.getContext()));
                break;
            case ResourceTable.Integer_action_main_fab_background_color_opened_orange:
                speedDialView.setMainFabOpenedBackgroundColor(UiUtils.getColor(speedDialView.getContext(), ResourceTable.Color_material_orange_500));
                break;
            case ResourceTable.Integer_action_main_fab_background_color_opened_purple:
                speedDialView.setMainFabOpenedBackgroundColor(UiUtils.getColor(speedDialView.getContext(), ResourceTable.Color_material_purple_500));
                break;
            case ResourceTable.Integer_action_main_fab_background_color_opened_white:
                speedDialView.setMainFabOpenedBackgroundColor(UiUtils.getColor(speedDialView.getContext(), ResourceTable.Color_material_white_1000));
                break;
            case ResourceTable.Integer_action_main_fab_background_color_opened_none:
                speedDialView.setMainFabOpenedBackgroundColor(0);
                break;
            default:
                break;
        }
    }

}
