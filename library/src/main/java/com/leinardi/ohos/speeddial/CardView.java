/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.leinardi.ohos.speeddial;

import ohos.agp.components.*;
import ohos.agp.components.element.Element;
import ohos.agp.render.Canvas;
import ohos.agp.utils.Color;
import ohos.app.Context;

public class CardView extends StackLayout implements Component.DrawTask, Component.BindStateChangedListener {
    private static final String fab_shadowCorner = "fab_shadowCorner";
    private static final String fab_showShadow = "fab_showShadow";
    private static final String fab_showShadowColor = "fab_showShadowColor";
    private static final String fab_shadowXOffset = "fab_shadowXOffset";
    private static final String fab_shadowYOffset = "fab_shadowYOffset";

    private boolean mShowShadow;
    private int mShadowCorner = AttrHelper.vp2px(4f, getContext());
    private int mShadowColor;
    private int mShadowXOffset = AttrHelper.vp2px(0f, getContext());
    private int mShadowYOffset = AttrHelper.vp2px(2f, getContext());

    public CardView(Context context) {
        super(context);
        initWithContext(context, null);
    }

    public CardView(Context context, AttrSet attrSet) {
        super(context, attrSet);
        initWithContext(context, attrSet);
    }

    public CardView(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        initWithContext(context, attrSet);
    }

    private void initWithContext(Context context, AttrSet attrSet) {
        setClipEnabled(false);
        mContext = context;
        if (attrSet != null) {
            mShadowCorner = AttrUtils.getDimensionValueByAttr(attrSet, fab_shadowCorner, mShadowCorner);
            mShowShadow = AttrUtils.getBooleanValueByAttr(attrSet, fab_showShadow, true);
            mShadowColor = AttrUtils.getColorValueByAttr(attrSet, fab_showShadowColor, new Color(0x66000000)).getValue();
            mShadowXOffset = AttrUtils.getDimensionValueByAttr(attrSet, fab_shadowXOffset, mShadowXOffset);
            mShadowYOffset = AttrUtils.getDimensionValueByAttr(attrSet, fab_shadowYOffset, mShadowYOffset);
        } else {
            mShowShadow = true;
            mShadowColor = 0x66000000;
        }

        setBindStateChangedListener(this);
        addDrawTask(this, Component.DrawTask.BETWEEN_BACKGROUND_AND_CONTENT);

    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
//        if(mShowShadow) {
//            Paint mShadowPaint = new Paint();
//            mShadowPaint.setColor(new Color(mShadowColor));
//            mShadowPaint.setAntiAlias(true);
//            mShadowPaint.setMaskFilter(new MaskFilter(AttrHelper.fp2px(6f, mContext), MaskFilter.Blur.NORMAL));
//
//            RectFloat rectFloat = new RectFloat(mShadowXOffset, mShadowYOffset, getWidth() - mShadowXOffset, getHeight() - mShadowYOffset);
//
//            canvas.drawRoundRect(rectFloat, mShadowCorner, mShadowCorner, mShadowPaint);
//        }
    }

    @Override
    public void setBackground(Element element) {
        super.setBackground(element);
    }

    @Override
    public void onComponentBoundToWindow(Component component) {
    }

    @Override
    public void onComponentUnboundFromWindow(Component component) {

    }
}
