/*
 * Copyright 2021 Roberto Leinardi.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.leinardi.ohos.speeddial;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.AttrSet;
import ohos.agp.components.DependentLayout;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.app.Context;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

@SuppressWarnings({"unused", "WeakerAccess"})
public class SpeedDialOverlayLayout extends DependentLayout {
    private static final HiLogLabel LABEL_LOG = new HiLogLabel(3, 0xD000F00, "SpeedDialOverlayLayout");

    private static final String TAG = SpeedDialOverlayLayout.class.getSimpleName();
    private boolean mClickableOverlay;
    private int mAnimationDuration;
    private ClickedListener mClickListener;

    public SpeedDialOverlayLayout(Context context) {
        super(context);
        init(context, null);
    }

    public SpeedDialOverlayLayout(Context context, AttrSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public SpeedDialOverlayLayout(Context context, AttrSet attrs, String styleName) {
        super(context, attrs, styleName);
        init(context, attrs);
    }

    public boolean hasClickableOverlay() {
        return mClickableOverlay;
    }

    /**
     * Enables or disables the click on the overlay view.
     *
     * @param clickableOverlay True to enable the click, false otherwise.
     */
    public void setClickableOverlay(boolean clickableOverlay) {
        mClickableOverlay = clickableOverlay;
        setClickedListener(mClickListener);
    }

    public void setAnimationDuration(int animationDuration) {
        mAnimationDuration = animationDuration;
    }

    public void show() {
        show(true);
    }

    public void show(boolean animate) {
        if (animate) {
            UiUtils.fadeInAnim(this);
        } else {
            setVisibility(VISIBLE);
        }
    }

    public void hide() {
        hide(true);
    }

    public void hide(boolean animate) {
        if (animate) {
            UiUtils.fadeOutAnim(this);
        } else {
            setVisibility(HIDE);
        }
    }

    @Override
    public void setClickedListener(ClickedListener listener) {
        mClickListener = listener;
        super.setClickedListener(hasClickableOverlay() ? listener : null);
    }

    private void init(Context context, AttrSet attrs) {
        int overlayColor = 0;
        try {
            overlayColor = UiUtils.getColor(context, ResourceTable.Color_sd_overlay_color);
            overlayColor = AttrUtils.getColorValueByAttr(attrs, "background_element", new Color(overlayColor)).getValue();
            mClickableOverlay = AttrUtils.getBooleanValueByAttr(attrs, "clickable_overlay", true);
        } catch (Exception e) {
            HiLog.error(LABEL_LOG, "Failure setting FabOverlayLayout attrs:"+e.getMessage());
        }
        //setElevation(getResources().getDimension(R.dimen.sd_overlay_elevation));
        ShapeElement bg = new ShapeElement();
        bg.setRgbColor(RgbColor.fromArgbInt(overlayColor));
        setBackground(bg);

        setVisibility(HIDE);
        mAnimationDuration = 500;
    }
}
