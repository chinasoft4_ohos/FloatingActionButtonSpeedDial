/*
 * Copyright 2021 Roberto Leinardi.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.leinardi.ohos.speeddial;

import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.ComponentParent;
import ohos.agp.utils.Matrix;
import ohos.agp.utils.Rect;
import ohos.agp.utils.RectFloat;

class ViewGroupUtils {
    private static final ThreadLocal<Matrix> MATRIX_THREAD_LOCAL = new ThreadLocal<>();
    private static final ThreadLocal<RectFloat> RECT_F = new ThreadLocal<>();

    private ViewGroupUtils() {
    }

    /**
     * This is a port of the common
     * from the framework, but adapted to take transformations into account. The result
     * will be the bounding rect of the real transformed rect.
     *
     * @param descendant view defining the original coordinate system of rect
     * @param rect       (in/out) the rect to offset from descendant to this view's coordinate system
     */
    static void offsetDescendantRect(ComponentContainer parent, Component descendant, Rect rect) {
        Matrix m = MATRIX_THREAD_LOCAL.get();
        if (m == null) {
            m = new Matrix();
            MATRIX_THREAD_LOCAL.set(m);
        } else {
            m.reset();
        }

        offsetDescendantMatrix(parent, descendant, m);

        RectFloat rectF = RECT_F.get();
        if (rectF == null) {
            rectF = new RectFloat();
            RECT_F.set(rectF);
        }
        rectF.modify(rect);
        m.mapRect(rectF);
        rect.set((int) (rectF.left + 0.5f), (int) (rectF.top + 0.5f),
                (int) (rectF.right + 0.5f), (int) (rectF.bottom + 0.5f));
    }

    /**
     * Retrieve the transformed bounding rect of an arbitrary descendant view.
     * This does not need to be a direct child.
     *
     * @param descendant descendant view to reference
     * @param out        rect to set to the bounds of the descendant view
     */
    static void getDescendantRect(ComponentContainer parent, Component descendant, Rect out) {
        out.set(0, 0, descendant.getWidth(), descendant.getHeight());
        offsetDescendantRect(parent, descendant, out);
    }

    private static void offsetDescendantMatrix(ComponentContainer target, Component view, Matrix m) {
        final ComponentParent parent = view.getComponentParent();
        if (parent instanceof Component && parent != target) {
            final Component vp = (Component) parent;
            offsetDescendantMatrix(target, vp, m);
            m.preTranslate(-vp.getScrollValue(Component.AXIS_X), -vp.getScrollValue(Component.AXIS_Y));
        }

        m.preTranslate(view.getLeft(), view.getTop());

//        if (!view.getMatrix().isIdentity()) {
//            m.preConcat(view.getMatrix());
//        }
    }
}
